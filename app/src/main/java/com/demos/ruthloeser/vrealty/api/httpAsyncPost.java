
package com.demos.ruthloeser.vrealty.api;

import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class httpAsyncPost extends httpAsyncBase {

    private JSONObject _json;
    private httpResponder _responder;

    public httpAsyncPost(JSONObject json, httpResponder responder ){
        _json = json;
        _responder = responder;
    }

    private void httpPost(final String _url, final JSONObject _json, final httpResponder responder) {
                try
                {
                    System.setProperty("http.agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.29 Safari/537.36");
                    URL url = new URL(_url);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    if (_cookiesHeader!=null)
                        load_cookies_into_connection(conn);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setDoOutput(true);
                    conn.setDoInput(true);
                    JSONObject jsonParam = _json;
                    Log.i("JSON", jsonParam.toString());
                    DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                    //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                    os.writeBytes(jsonParam.toString());
                    os.flush();
                    os.close();
                    int responseCode = conn.getResponseCode();
                    Log.i("STATUS", String.valueOf(responseCode));
                    Log.i("MSG", conn.getResponseMessage());
                    if (responseCode >=400) {
                        responder.onHttpError(conn);
                        conn.disconnect();
                        return;
                    }
                    String json_response = "";
                    InputStreamReader in = new InputStreamReader(conn.getInputStream());
                    BufferedReader br = new BufferedReader(in);
                    String text;
                    while ((text = br.readLine()) != null) {
                        json_response += text;
                    }

                    if (_cookiesHeader==null)
                        save_cookies_from_connection(conn);

                    responder.onDataReceived(json_response);
                    conn.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
    }


    @Override
    protected Integer doInBackground(String... params) {
        httpPost(params[0],_json,_responder);
        return 0;
    }


    @Override
    protected void onPostExecute(Integer result) {
    }
}


