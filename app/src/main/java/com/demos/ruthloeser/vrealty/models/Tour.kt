package com.demos.ruthloeser.vrealty.models

import java.io.Serializable

/**
 * Created by ruthloeser on 14/09/2017.
 */
data class Tour(val id : Int,
                val title : String,
                val user : Int,
                val root_index : Int,
                val start_index : Int,
                val last_publish : String,
                val publish_needed : Boolean,
                val rooms : ArrayList<Room>?,
                val created : String) : Serializable