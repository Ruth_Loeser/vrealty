
package com.demos.ruthloeser.vrealty.api;
/////////////////////////////////////

import android.os.AsyncTask;

import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;

public abstract class httpAsyncBase  extends AsyncTask<String, Void, Integer> {
    private static final String COOKIES_HEADER = "Set-Cookie";
    static List<String> _cookiesHeader;

    @Override
    abstract protected Integer doInBackground(String... params);

    public static void clear_cookies()
    {
        _cookiesHeader = null;
    }

    static void save_cookies_from_connection(HttpURLConnection con){
        HttpURLConnection connection = con;
        Map<String, List<String>> headerFields = connection.getHeaderFields();
        List<String> cookiesHeader = headerFields.get(COOKIES_HEADER);
        _cookiesHeader = cookiesHeader;
    }

    static void load_cookies_into_connection(HttpURLConnection con){
        String clean_csrf="";
        String clean_sessionid="";
        String csrf_token = "";

        for (String cookie : _cookiesHeader) {
            if (cookie.contains("csrf")) {
                clean_csrf = cookie.split(";", 2)[0];
                csrf_token = clean_csrf.split("=",2)[1];
            } else if (cookie.contains("session"))
                clean_sessionid = cookie.split(";", 2)[0];
        }

        String full_clean_cookie =clean_csrf+";"+clean_sessionid;
        System.out.println(full_clean_cookie );
        con.setRequestProperty("Cookie", full_clean_cookie );
        con.setRequestProperty("X-CSRFToken", csrf_token);
    }
}


