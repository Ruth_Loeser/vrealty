package com.demos.ruthloeser.vrealty.controllers;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.demos.ruthloeser.vreality.R;
import com.demos.ruthloeser.vrealty.network.HttpConnector;
import com.demos.ruthloeser.vrealty.network.HttpDownloadListener;
import com.demos.ruthloeser.vrealty.network.ImageData;
import com.google.vr.sdk.widgets.pano.VrPanoramaEventListener;
import com.google.vr.sdk.widgets.pano.VrPanoramaView;

public class CaptureResultActivity extends AppCompatActivity {

    public static final String LATEST_CAPTURE_FIELD_ID = "LATEST_CAPTURE_FIELD_ID";
    public static final int SELECT_GALLERY_IMAGE_REQUEST =4563;
    private static final String TAG = PanoramaActivity.class.getSimpleName();
    private VrPanoramaView mPanoramaView;
    private VrPanoramaView.Options mPanoOptions = new VrPanoramaView.Options();


    ProgressDialog progressDialog;


    public static Bitmap Last_Saved_Bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_result);
        Last_Saved_Bitmap = null;
        mPanoramaView = (VrPanoramaView) findViewById(R.id.panoramaCaptureView);
        mPanoramaView.setInfoButtonEnabled(false);
        mPanoramaView.setStereoModeButtonEnabled(false);
        mPanoramaView.setFullscreenButtonEnabled(false);

        progressDialog = new ProgressDialog(this);

        String picID = getIntent().getStringExtra(LATEST_CAPTURE_FIELD_ID);

        if (picID == null){
            Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
            getIntent.setType("image/*");

            Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickIntent.setType("image/*");

            Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

            startActivityForResult(chooserIntent, SELECT_GALLERY_IMAGE_REQUEST);

        }
        else {
            new LoadPhotoTask(
                    getResources().getString(R.string.theta_ip_address),
                    picID
            ).execute();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == SELECT_GALLERY_IMAGE_REQUEST) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            Last_Saved_Bitmap = BitmapFactory.decodeFile(picturePath);

            new ImageLoaderTask().execute(Last_Saved_Bitmap);

//
//            final Bundle extras = data.getExtras();
//            if (extras != null) {
//                //Get image
//                Last_Saved_Bitmap = extras.getParcelable("data");
//                new ImageLoaderTask().execute(Last_Saved_Bitmap);
////                setResult(RESULT_OK);
////                finish();
//            }
        }
    }


    public void cancel(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }

    public void use(View view) {
        if (Last_Saved_Bitmap == null)
            return;

        setResult(RESULT_OK);
        finish();

    }

    private class LoadPhotoTask extends AsyncTask<Void, Object, ImageData> {

        //        private LogView logViewer;
        private String cameraIpAddress;
        private String fileId;
        private long fileSize;
        private long receivedDataSize = 0;

        public LoadPhotoTask(String cameraIpAddress, String fileId) {
//            this.logViewer = (LogView) findViewById(R.id.photo_info);
            this.cameraIpAddress = cameraIpAddress;
            this.fileId = fileId;
        }

        @Override
        protected void onPreExecute() {
            progressDialog.setMessage("download photo");
            progressDialog.show();
        }

        @Override
        protected ImageData doInBackground(Void... params) {
            try {
                publishProgress("start to download image" + fileId);
                HttpConnector camera = new HttpConnector(cameraIpAddress);
                ImageData resizedImageData = camera.getImage(fileId, new HttpDownloadListener() {
                    @Override
                    public void onTotalSize(long totalSize) {
                        fileSize = totalSize;
                    }

                    @Override
                    public void onDataReceived(int size) {
                        receivedDataSize += size;

                        if (fileSize != 0) {
                            int progressPercentage = (int) (receivedDataSize * 100 / fileSize);
                            publishProgress(progressPercentage);
                        }
                    }
                });
                publishProgress("finish to download");

                return resizedImageData;

            } catch (Throwable throwable) {
                String errorLog = Log.getStackTraceString(throwable);
                publishProgress(errorLog);
                return null;
            }
        }

        @Override
        protected void onProgressUpdate(Object... values) {
            for (Object param : values) {
                if (param instanceof Integer) {

                    progressDialog.setMessage("DownLoadPhotoTask:onProgressUpdate-"+(Integer) param + "%");
//                    progressDialog.setMessage("downloading photo");
                    setProgress((Integer) param);
//                    progressBar.setProgress((Integer) param);
                } else if (param instanceof String) {
                    progressDialog.setMessage("DownLoadPhotoTask:onProgressUpdate-"+(String) param);
                    Log.i(TAG, "DownLoadPhotoTask:onProgressUpdate-"+(String) param);
                }
            }
        }

        @Override
        protected void onPostExecute(ImageData imageData) {
            if (imageData != null) {

                byte[] dataObject = imageData.getRawData();

                if (dataObject == null) {
                    Log.i(TAG, "LoadPhotoTask:onPostExecute-failed to download image dataObject == null)");
                    return;
                }

                Bitmap __bitmap = BitmapFactory.decodeByteArray(dataObject, 0, dataObject.length);


                if (__bitmap == null)
                    progressDialog.hide();
//                    progressBar.setVisibility(View.GONE);
                else {
                    new ImageLoaderTask().execute(__bitmap);

//                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                    __bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    Last_Saved_Bitmap = __bitmap;// stream.toByteArray();
                }

                Double yaw = imageData.getYaw();
                Double pitch = imageData.getPitch();
                Double roll = imageData.getRoll();
//                logViewer.append("<Angle: yaw=" + yaw + ", pitch=" + pitch + ", roll=" + roll + ">");

//                mTexture = new Photo(__bitmap, yaw, pitch, roll);
//                if (null != mGLPhotoView) {
//                    mGLPhotoView.setTexture(mTexture);
//                }
            } else {
                Log.i(TAG, "LoadPhotoTask:onPostExecute-failed to download image imageData == null");
            }
        }
    }

    class ImageLoaderTask extends AsyncTask<Bitmap, Void, Boolean> {

        /**
         * Reads the bitmap from disk in the background and waits until it's loaded by pano widget.
         */
        @Override
        protected Boolean doInBackground(Bitmap... image) {
            VrPanoramaView.Options panoOptions = null;  // It's safe to use null VrPanoramaView.Options.
//            InputStream istr = null;
            AssetManager assetManager = getAssets();
//            try {
//                istr = assetManager.open("sphere.jpg");
                panoOptions = new VrPanoramaView.Options();
                panoOptions.inputType = VrPanoramaView.Options.TYPE_MONO;
//            } catch (IOException e) {
//                Log.e(TAG, "Could not decode default bitmap: " + e);
//                return false;
//            }

            mPanoramaView.loadImageFromBitmap(image[0], panoOptions);
//            try {
//                istr.close();
//            } catch (IOException e) {
//                Log.e(TAG, "Could not close input stream: " + e);
//            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean inputStream) {

            progressDialog.hide();
            mPanoramaView.setZ(100f);
        }
    }


    /**
     * Listen to the important events from widget.
     */
    private class ActivityPanoramaEventListener extends VrPanoramaEventListener {
        /**
         * Called by pano widget on the UI thread when it's done loading the image.
         */
        @Override
        public void onLoadSuccess() {
        }

        /**
         * Called by pano widget on the UI thread on any asynchronous error.
         */
        @Override
        public void onLoadError(String errorMessage) {
//            loadImageSuccessful = false;
            Toast.makeText(
                    CaptureResultActivity.this, "Error loading pano: " + errorMessage, Toast.LENGTH_LONG)
                    .show();
            Log.e(TAG, "Error loading pano: " + errorMessage);
        }
    }


}
