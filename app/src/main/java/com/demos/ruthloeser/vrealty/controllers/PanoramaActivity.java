package com.demos.ruthloeser.vrealty.controllers;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.demos.ruthloeser.vreality.R;
import com.demos.ruthloeser.vrealty.Utils.FileUtils;
import com.google.vr.sdk.widgets.pano.VrPanoramaEventListener;
import com.google.vr.sdk.widgets.pano.VrPanoramaView;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by ruthloeser on 24/07/2017.
 */

public class PanoramaActivity extends FragmentActivity {
    private static final int CAPTURE_APPROVE_REQUEST = 24356;

    public static final String ROOM_NAME_EXTRA = "ROOM_NAME_EXTRA";
    public static final String CHOOSE_GALLERY = "CHOOSE_GALLERY";


    private static final String TAG = PanoramaActivity.class.getSimpleName();
    private VrPanoramaView mPanoramaView;
    private VrPanoramaView.Options mPanoOptions = new VrPanoramaView.Options();
    private PanoramaActivity.ImageLoaderTask mBackgroundImageLoaderTask;
    private Button mBtnShoot;


    private Camera360Fragment m360Fragment;
    private ProgressDialog progressDialog;

    private String mRoomName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.panorama_activity);
        mRoomName = getIntent().getStringExtra(ROOM_NAME_EXTRA);

        mPanoramaView = (VrPanoramaView) findViewById(R.id.panoramaView);

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.

//        mBtnShoot = (Button) findViewById(R.id.dummy_button);

//        findViewById(R.id.dummy_button).setOnTouchListener(mDelayHideTouchListener);


        progressDialog = new ProgressDialog(this);
        m360Fragment = (Camera360Fragment) getSupportFragmentManager().findFragmentById(R.id.camera_360_fragment);


        mPanoramaView.setEventListener(new PanoramaActivity.ActivityPanoramaEventListener());
        mPanoramaView.setInfoButtonEnabled(false);
        mPanoramaView.setStereoModeButtonEnabled(false);
        mPanoramaView.setFullscreenButtonEnabled(false);


        mPanoOptions.inputType = VrPanoramaView.Options.TYPE_MONO;// Load the bitmap in a background thread to avoid blocking the UI thread. This operation can
        // take 100s of milliseconds.
        // Load the bitmap in a background thread to avoid blocking the UI thread. This operation can
        // take 100s of milliseconds.

        if (mBackgroundImageLoaderTask != null) {
            // Cancel any task from a previous intent sent to this activity.
            mBackgroundImageLoaderTask.cancel(true);
        }

        if (getIntent().getBooleanExtra(CHOOSE_GALLERY, false)) {
            continueAfterCapture(null);
        }
        else {

            mBackgroundImageLoaderTask = new PanoramaActivity.ImageLoaderTask();
            mBackgroundImageLoaderTask.execute();
        }

    }

    public void continueAfterCapture(String latestCapturedFileId) {
        Intent intent = new Intent(this, CaptureResultActivity.class);
        intent.putExtra(CaptureResultActivity.LATEST_CAPTURE_FIELD_ID, latestCapturedFileId);
        startActivityForResult(intent, CAPTURE_APPROVE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_APPROVE_REQUEST) {
            if (resultCode == RESULT_OK) {
                final AlertDialog.Builder builderSingle = new AlertDialog.Builder(PanoramaActivity.this);
                builderSingle.setIcon(R.drawable.capture);
                builderSingle.setTitle("Select One Name:");

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(PanoramaActivity.this, android.R.layout.select_dialog_singlechoice);

                String[] roomsArray = getResources().getStringArray(R.array.rooms_name);

                arrayAdapter.addAll(roomsArray);

                builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mRoomName = arrayAdapter.getItem(which);

                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {

                                String filePath = FileUtils.saveLargeImage(PanoramaActivity.this, CaptureResultActivity.Last_Saved_Bitmap, mRoomName);
//                                FileUtils.doIt(PanoramaActivity.this, filePath);
                            }
                        });
                        thread.start();


                        new ImageLoaderTask().execute(CaptureResultActivity.Last_Saved_Bitmap);
//                        AlertDialog.Builder builderInner = new AlertDialog.Builder(PanoramaActivity.this);
//                        builderInner.setMessage(strName);
//                        builderInner.setTitle("Your Selected Item is");
//                        builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog,int which) {
//                                dialog.dismiss();
//                            }
//                        });
//                        builderInner.show();
                    }
                });
                builderSingle.show();
            }
            else if (resultCode == RESULT_CANCELED){
                ///// resuming
            }
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        progressDialog.hide();
    }


    public void showProgress(final String s) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                progressDialog.setMessage(s);
                progressDialog.show();
            }
        });
    }

    public void hideProgress() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                progressDialog.hide();
            }
        });
    }


    class ImageLoaderTask extends AsyncTask<Bitmap, Void, Boolean> {

        /**
         * Reads the bitmap from disk in the background and waits until it's loaded by pano widget.
         */
        @Override
        protected Boolean doInBackground(Bitmap... readyImage) {
            VrPanoramaView.Options panoOptions = null;  // It's safe to use null VrPanoramaView.Options.
            InputStream istr = null;
//            if (fileInformation == null || fileInformation.length < 1
//                    || fileInformation[0] == null || fileInformation[0].first == null) {
            AssetManager assetManager = getAssets();
            Bitmap roomBmp = null;
            if (readyImage != null && readyImage.length > 0 && readyImage[0] != null)
            {
                roomBmp = readyImage[0];
            }
            else {
                try {
                    if (mRoomName != null)
                        roomBmp = FileUtils.loadLargeImage(PanoramaActivity.this, mRoomName);
                    else
                        istr = assetManager.open("sphere.jpg");
                    panoOptions = new VrPanoramaView.Options();
                    panoOptions.inputType = VrPanoramaView.Options.TYPE_MONO;
                } catch (IOException e) {
                    Log.e(TAG, "Could not decode default bitmap: " + e);
                    return false;
                }
            }
//            } else {
//                try {
//                    istr = new FileInputStream(new File(fileInformation[0].first.getPath()));
//                    panoOptions = fileInformation[0].second;
//                } catch (IOException e) {
//                    Log.e(TAG, "Could not load file: " + e);
//                    return false;
//                }
//            }

            if (mRoomName != null && roomBmp != null)
                mPanoramaView.loadImageFromBitmap(roomBmp, panoOptions);
            else {
                mPanoramaView.loadImageFromBitmap(BitmapFactory.decodeStream(istr), panoOptions);
                try {
                    istr.close();
                } catch (IOException e) {
                    Log.e(TAG, "Could not close input stream: " + e);
                }
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean inputStream) {
            mPanoramaView.setZ(100f);
        }
    }


    /**
     * Listen to the important events from widget.
     */
    private class ActivityPanoramaEventListener extends VrPanoramaEventListener {
        /**
         * Called by pano widget on the UI thread when it's done loading the image.
         */
        @Override
        public void onLoadSuccess() {
//            loadImageSuccessful = true;
        }

        /**
         * Called by pano widget on the UI thread on any asynchronous error.
         */
        @Override
        public void onLoadError(String errorMessage) {
//            loadImageSuccessful = false;
            Toast.makeText(
                    PanoramaActivity.this, "Error loading pano: " + errorMessage, Toast.LENGTH_LONG)
                    .show();
            Log.e(TAG, "Error loading pano: " + errorMessage);
        }
    }
}
