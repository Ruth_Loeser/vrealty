package com.demos.ruthloeser.vrealty.controllers;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.demos.ruthloeser.vreality.R;
import com.demos.ruthloeser.vrealty.network.HttpConnector;
import com.demos.ruthloeser.vrealty.viewes.MJpegInputStream;
import com.demos.ruthloeser.vrealty.viewes.MJpegView;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by ruthloeser on 22/07/2017.
 */

public class Camera360Fragment extends Fragment {

    private ShowLiveViewTask mLivePreviewTask = null;
    private MJpegView mMv;
    private TextView mThetaMessageTextView;
    private String mCameraIpAddress;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.camera_360_fragment, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMv = (MJpegView) getView().findViewById(R.id.live_view);
        mCameraIpAddress = getResources().getString(R.string.theta_ip_address);
        mThetaMessageTextView = (TextView) getView().findViewById(R.id.liveThetaMessage);

    }


    @Override
    public void onResume() {
        super.onResume();
        mMv.play();

        if (mLivePreviewTask != null) {
            mLivePreviewTask.cancel(true);
        }
        mLivePreviewTask = new ShowLiveViewTask();
        mLivePreviewTask.execute(mCameraIpAddress);

    }

    @Override
    public void onPause() {
        super.onPause();
        mMv.stopPlay();
    }


    private class ShowLiveViewTask extends AsyncTask<String, String, MJpegInputStream> {
        @Override
        protected MJpegInputStream doInBackground(String... ipAddress) {
            MJpegInputStream mjis = null;
            final int MAX_RETRY_COUNT = 20;

            for (int retryCount = 0; retryCount < MAX_RETRY_COUNT; retryCount++) {
                try {
                    publishProgress("start Live view");
                    HttpConnector camera = new HttpConnector(ipAddress[0]);
                    InputStream is = camera.getLivePreview();
                    mjis = new MJpegInputStream(is);
                    retryCount = MAX_RETRY_COUNT;
                } catch (IOException e) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e1) {
//                        e1.printStackTrace();
                    }
                } catch (JSONException e) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e1) {
//                        e1.printStackTrace();
                    }
                }
            }

            return mjis;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            for (String log : values) {
                mThetaMessageTextView.append(log);
            }
        }

        @Override
        protected void onPostExecute(MJpegInputStream mJpegInputStream) {
            if (mJpegInputStream != null) {
                mMv.setSource(mJpegInputStream);
                mMv.setZ(0f);
            } else {
                mThetaMessageTextView.append("failed to start live view");
//                logViewer.append("failed to start live view");
            }
        }
    }



}
