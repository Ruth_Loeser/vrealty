package com.demos.ruthloeser.vrealty.models

import android.support.annotation.Nullable
import java.io.Serializable

/**
 * Created by ruthloeser on 21/07/2017.
 */
data class Room(val id : Int,
                val title: String,
                val bg_image_url: String,
                val bg_image_uploaded: Boolean,
                val hotspots: ArrayList<HotSpot>?) : Serializable