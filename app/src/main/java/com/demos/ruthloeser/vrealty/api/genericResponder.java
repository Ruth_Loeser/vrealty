package com.demos.ruthloeser.vrealty.api;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

public class genericResponder implements httpResponder {
    public  genericResponder(){
        _hasValidJSONData = false;
        _jsonData = null;
    }

    public void onHttpError (HttpURLConnection con) {
        try
        {
            int responseCode = con.getResponseCode();
            InputStreamReader in = new InputStreamReader(con.getErrorStream());
            BufferedReader br = new BufferedReader(in);
            String text;
            String str_error="";
            while ((text = br.readLine()) != null) {
                str_error+= text;
            }
            System.out.println(str_error);
            System.out.println("HTTP error "+responseCode );
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @Override
    public void onDataReceived(String data) {
        System.out.println("genericResponder::onDataReceived");
        String json = data;
        _rawData = data;
        _hasValidJSONData = false;
        try {
            JSONObject obj2 = new JSONObject(json);
            System.out.println("genericResponder::onDataReceived JSON response "+obj2.toString());
            _jsonData = obj2;
            _hasValidJSONData = true;
        }
        catch (Throwable t) {
            try {
                JSONArray jr = new JSONArray(json);
                JSONObject jo = jr.getJSONObject(0);
                System.out.println("genericResponder::onDataReceived JSON response (Array Detected)"+jo.toString());
                _jsonData = jo;
                _hasValidJSONData = true;
            }
            catch (Exception e) {
                System.out.println("genericResponder::onDataReceived Could not parse malformed JSON: \""+ json + "\"");
            }
        }
        System.out.println("genericResponder::onDataReceived Raw Response"+ String.valueOf(data));
    }
    protected boolean      _hasValidJSONData;
    protected JSONObject _jsonData;
    protected String _rawData;
}