package com.demos.ruthloeser.vrealty.adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.demos.ruthloeser.vreality.R;
import com.demos.ruthloeser.vrealty.Utils.FileUtils;
import com.demos.ruthloeser.vrealty.controllers.RoomsActivity;
import com.demos.ruthloeser.vrealty.models.Room;

import java.util.ArrayList;

/**
 * Created by ruthloeser on 21/07/2017.
 */

public class RoomsAdapter extends RecyclerView.Adapter<RoomsAdapter.RoomItemViewHolder> {

    private final RoomsActivity mActivity;
    private ArrayList<Room> mRoomList;

    public RoomsAdapter(ArrayList<Room> rooms, RoomsActivity activity) {
        mRoomList = new ArrayList<>(rooms);
        mActivity = activity;
    }

    @Override
    public RoomItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_list_item, parent, false);

        // set the view's size, margins, paddings and layout parameters
        RoomItemViewHolder vh = new RoomItemViewHolder(mView);
        return vh;
    }

    @Override
    public void onBindViewHolder(RoomItemViewHolder holder, int position) {
        Room room = mRoomList.get(position);
        holder.roomNameTV.setText(room.getTitle());
        String hotSpotsText;
        if (room.getHotspots() == null || room.getHotspots().size() == 0)
            hotSpotsText = "no hotspots";
        else if (room.getHotspots().size() == 1)
            hotSpotsText = "1 hotspot not connected";
        else
            hotSpotsText = room.getHotspots().size()+ " hotspots not connected";

        holder.hotspotsTV.setText(hotSpotsText);

        String path = FileUtils.getLargeImagePath(mActivity, room.getTitle());
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        Bitmap bitmap = BitmapFactory.decodeFile(path, options);// BitmapFactory.decodeStream(large, null, options);

        holder.imageView.setImageBitmap(bitmap);
        holder.setPosition(position);
    }

    @Override
    public int getItemCount() {
        return (mRoomList == null)? 0: mRoomList.size();
    }

    public class RoomItemViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView roomNameTV;
        TextView hotspotsTV;
        int position;
        RelativeLayout selectLayout;
        FrameLayout deleteLayout;
        public RoomItemViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.roomThumbnail);
            roomNameTV = (TextView) itemView.findViewById((R.id.roomName));
            hotspotsTV = (TextView) itemView.findViewById((R.id.hotspotsText));
            selectLayout = (RelativeLayout) itemView.findViewById(R.id.item_layout);
            deleteLayout = (FrameLayout) itemView.findViewById(R.id.delete_layout);

            selectLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mActivity.roomSelected(position);
                }
            });

            deleteLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mActivity.roomDeleted(position);
//                    mRoomList.remove(getAdapterPosition());
//                    notifyItemRemoved(getAdapterPosition());
                }
            });


        }

        public void setPosition(int position) {
            this.position = position;
        }
    }

}
