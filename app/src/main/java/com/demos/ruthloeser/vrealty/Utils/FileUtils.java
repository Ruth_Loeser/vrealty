package com.demos.ruthloeser.vrealty.Utils;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by ruthloeser on 05/08/2017.
 */

public class FileUtils {

    public static  final String LARGE_IMAGES_FOLDER = "largeImageDir";
    public static  final String SMALL_IMAGES_FOLDER = "smallImageDir";


    public static String saveLargeImage(Context context, Bitmap bitmapImage, String roomName){
        return saveToInternalStorage(context, bitmapImage, LARGE_IMAGES_FOLDER, roomName + ".jpg");
    }

    public static String saveThumbnailImage(Context context, Bitmap bitmapImage, String roomName){
        return saveToInternalStorage(context, bitmapImage, SMALL_IMAGES_FOLDER, roomName + ".jpg");
    }


static TransferListener transferListener = new TransferListener() {
    public void onStateChanged(int id, String newState) {
        Log.i("transferObserver", "onStateChanged id="+id +" newState="+newState);



    }

    @Override
    public void onStateChanged(int id, TransferState state) {
        Log.i("transferObserver", "onStateChanged id="+id +" state="+state.name());

    }

    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
        Log.i("transferObserver", "onStateChanged id="+id +" bytesCurrent="+bytesCurrent +" bytesTotal="+bytesTotal);
    }

    public void onError(int id, Exception e) {
        Log.i("transferObserver", "onStateChanged id="+id +" error="+e.toString());
    }
};

    public static void doIt(Context context, String path) {

            BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAIC55GAAJWJIBC5TQ", "aHnTSXHev5yK253mxUMRMB1WFh+0ZR72x4kjiqkO");

            AmazonS3 s3 = new AmazonS3Client(awsCreds);
            TransferUtility transferUtility = new TransferUtility(s3, context.getApplicationContext());

        File file = new File(path);
        boolean i = file.exists();

            TransferObserver transferObserver = transferUtility.upload("vrealty","rFile", file);
        transferObserver.setTransferListener(new TransferListener() {
            public void onStateChanged(int id, String newState) {
                Log.i("transferObserver", "onStateChanged id="+id +" newState="+newState);

            }

            @Override
            public void onStateChanged(int id, TransferState state) {
                Log.i("transferObserver", "onStateChanged id="+id +" state="+state.name());

            }

            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                Log.i("transferObserver", "onStateChanged id="+id +" bytesCurrent="+bytesCurrent +" bytesTotal="+bytesTotal);
            }

            public void onError(int id, Exception e) {
                Log.i("transferObserver", "onStateChanged id="+id +" error="+e.toString());
            }
        });



        transferUtility.resume(transferObserver.getId());


        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//
//
        int x = 1;
    }

    private static String saveToInternalStorage(Context context, Bitmap bitmapImage, String dir, String fileName){
        ContextWrapper cw = new ContextWrapper(context.getApplicationContext());
        File directory = cw.getDir(dir, Context.MODE_PRIVATE);

        File mypath=new File(directory,fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mypath.getAbsolutePath();
    }

    public static File[] getAllSavedRooms(Context context) {
        ContextWrapper cw = new ContextWrapper(context.getApplicationContext());
        File directory = cw.getDir(LARGE_IMAGES_FOLDER, Context.MODE_PRIVATE);

        return directory.listFiles();
    }



    public static Bitmap loadThumbnail(Context context, String roomName) {
        ContextWrapper cw = new ContextWrapper(context.getApplicationContext());
        File directory = cw.getDir(SMALL_IMAGES_FOLDER, Context.MODE_PRIVATE);

        File imageFile = new File(directory, roomName + ".jpg");

        if (imageFile == null || !imageFile.exists())
            loadLargeImage(context, roomName);

        return loadImageFromStorage(imageFile.getAbsolutePath());
    }

    public static Bitmap loadLargeImage(Context context, String roomName) {
        ContextWrapper cw = new ContextWrapper(context.getApplicationContext());
        File directory = cw.getDir(LARGE_IMAGES_FOLDER, Context.MODE_PRIVATE);

        File imageFile = new File(directory, roomName + ".jpg");

        if (imageFile == null || !imageFile.exists())
            return null;

        return loadImageFromStorage(imageFile.getAbsolutePath());
    }

    public static String getLargeImagePath(Context context, String roomName) {
        ContextWrapper cw = new ContextWrapper(context.getApplicationContext());
        File directory = cw.getDir(LARGE_IMAGES_FOLDER, Context.MODE_PRIVATE);

        File imageFile = new File(directory, roomName + ".jpg");

        if (imageFile == null || !imageFile.exists())
            return null;

        return imageFile.getAbsolutePath();
    }


    private static Bitmap loadImageFromStorage(String path)
    {
        Bitmap b = null;
        try {
            File f=new File(path);
            b = BitmapFactory.decodeStream(new FileInputStream(f));
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        return b;

    }

    public static boolean deleteRoom(Context context, String roomName) {
        ContextWrapper cw = new ContextWrapper(context.getApplicationContext());
        File directory = cw.getDir(LARGE_IMAGES_FOLDER, Context.MODE_PRIVATE);

        File mypath=new File(directory,roomName + ".jpg");
        if (mypath != null && mypath.exists())
            return mypath.delete();

        return false;
    }


}
