package com.demos.ruthloeser.vrealty.controllers;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

import com.demos.ruthloeser.vreality.R;
import com.demos.ruthloeser.vrealty.Utils.FileUtils;
import com.demos.ruthloeser.vrealty.adapters.RoomsAdapter;
import com.demos.ruthloeser.vrealty.models.Room;


import java.io.File;
import java.util.ArrayList;

/**
 * Created by ruthloeser on 21/07/2017.
 */

public class RoomsActivity extends AppCompatActivity
{

    ArrayList<Room> rooms;
    RoomsAdapter roomsAdapter;
    RecyclerView roomsRecyclerView;
    SwipeRefreshLayout swipeRefreshLayout;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rooms_activity);

        roomsRecyclerView = (RecyclerView) findViewById(R.id.roomsRecyclerView);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView


        roomsRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

//        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(removeCallback);
//        itemTouchHelper.attachToRecyclerView(roomsRecyclerView);

        roomsRecyclerView.setHasFixedSize(true);
        roomsRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        rooms = new ArrayList<>();

        File[] files360 = FileUtils.getAllSavedRooms(this);

        if (files360 != null) {
            for (File file: files360) {
                String fname = file.getName();
                int pos = fname.lastIndexOf(".");
                if (pos > 0) {
                    fname = fname.substring(0, pos);
                }

                rooms.add(new Room(0,fname, file.getName(), false, null));
            }
        }


        roomsAdapter = new RoomsAdapter(rooms, this);
        roomsRecyclerView.setAdapter(roomsAdapter);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                rooms = new ArrayList<>();
                File[] files360 = FileUtils.getAllSavedRooms(RoomsActivity.this);

                if (files360 != null) {
                    for (File file: files360) {
                        String fname = file.getName();
                        int pos = fname.lastIndexOf(".");
                        if (pos > 0) {
                            fname = fname.substring(0, pos);
                        }
                        rooms.add(new Room(0, fname,file.getName(), false, null));
                    }
                }

                roomsAdapter = new RoomsAdapter(rooms, RoomsActivity.this);
                roomsRecyclerView.setAdapter(roomsAdapter);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private ItemTouchHelper.SimpleCallback removeCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
            final int position = viewHolder.getAdapterPosition(); //get position which is swipe

            if (direction == ItemTouchHelper.LEFT) {    //if swipe left

                AlertDialog.Builder builder = new AlertDialog.Builder(RoomsActivity.this); //alert for confirm to delete
                builder.setMessage("Are you sure to delete?");    //set message

                builder.setPositiveButton("REMOVE", new DialogInterface.OnClickListener() { //when click on DELETE
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FileUtils.deleteRoom(RoomsActivity.this, rooms.get(position).getTitle());
                        rooms.remove(position);  //then remove item
//                        toursAdapter.notifyItemRemoved(position);    //item removed from recylcerview
//                        toursAdapter.notifyItemRangeChanged(position, toursAdapter.getItemCount());
                        roomsAdapter = new RoomsAdapter(rooms, RoomsActivity.this);
                        roomsRecyclerView.setAdapter(roomsAdapter);

                        return;
                    }
                }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {  //not removing items if cancel is done
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        roomsAdapter.notifyItemRemoved(position + 1);    //notifies the RecyclerView Adapter that data in adapter has been removed at a particular position.
                        roomsAdapter.notifyItemRangeChanged(position, roomsAdapter.getItemCount());   //notifies the RecyclerView Adapter that positions of element in adapter has been changed from position(removed element index to end of list), please update it.
                        return;
                    }
                }).show();  //show alert dialog
            }
        }
    };



    @Override
    protected void onResume() {
        super.onResume();

//        tours = new ArrayList<>();
//
//        File[] files360 = FileUtils.getAllSavedRooms(this);
//
//        if (files360 != null) {
//            for (File file: files360) {
//                String fname = file.getTitle();
//                int pos = fname.lastIndexOf(".");
//                if (pos > 0) {
//                    fname = fname.substring(0, pos);
//                }
//
//                tours.add(new Room(fname, null, file.getTitle()));
//            }
//        }
//
//        toursAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.rooms_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add_room) {
            final AlertDialog.Builder builderSingle = new AlertDialog.Builder(RoomsActivity.this);
            builderSingle.setIcon(R.drawable.capture);
            builderSingle.setTitle("Select One Name:");

            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(RoomsActivity.this, android.R.layout.select_dialog_singlechoice);

            arrayAdapter.addAll(new String[] {"Camera", "Photo Library"});

            builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String choice = arrayAdapter.getItem(which);
                    Intent intent = new Intent(RoomsActivity.this, PanoramaActivity.class);
                    intent.putExtra(PanoramaActivity.CHOOSE_GALLERY,!choice.equals("Camera"));
                    startActivity(intent);
                }
            });
            builderSingle.show();
            return true;


        }

        return super.onOptionsItemSelected(item);
    }


    public void roomDeleted(int position){
        FileUtils.deleteRoom(RoomsActivity.this, rooms.get(position).getTitle());
//        mRoomList.remove(getAdapterPosition());
//        notifyItemRemoved(getAdapterPosition());
        rooms.remove(position);  //then remove item
        roomsAdapter.notifyItemRemoved(position);    //item removed from recylcerview
        roomsAdapter.notifyItemRangeChanged(position, roomsAdapter.getItemCount()+1);
//        toursAdapter = new RoomsAdapter(tours, RoomsActivity.this);
//        roomsRecyclerView.setAdapter(toursAdapter);
    }

    public void roomSelected(int position) {
        Room room = rooms.get(position);
        Intent intent = new Intent(this, PanoramaActivity.class);
        intent.putExtra(PanoramaActivity.ROOM_NAME_EXTRA, room.getTitle());
        startActivity(intent);
    }
}
