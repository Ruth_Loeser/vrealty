package com.demos.ruthloeser.vrealty.controllers;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.demos.ruthloeser.vreality.R;
import com.demos.ruthloeser.vrealty.network.HttpConnector;
import com.demos.ruthloeser.vrealty.network.HttpEventListener;

import java.io.ByteArrayOutputStream;

/**
 * Created by ruthloeser on 24/07/2017.
 */

public class CaptureFragment extends Fragment {

    private static final String TAG = CaptureFragment.class.getSimpleName();

    private ImageButton mCaptureButton;
    private String mCameraIpAddress;
    private PanoramaActivity mActivity;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mActivity = (PanoramaActivity) getActivity();
        return inflater.inflate(R.layout.camera_capture_fragment, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mCaptureButton = (ImageButton) getView().findViewById(R.id.captureButton);
        mCameraIpAddress = getResources().getString(R.string.theta_ip_address);


        mCaptureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ShootTask().execute();
            }
        });

    }



    private class ShootTask extends AsyncTask<Void, Void, HttpConnector.ShootResult> {

        @Override
        protected void onPreExecute() {
            mActivity.showProgress("Taking Picture");

            Log.i(TAG, "takePicture");
        }

        @Override
        protected HttpConnector.ShootResult doInBackground(Void... params) {
            CaptureListener postviewListener = new CaptureListener();
            HttpConnector camera = new HttpConnector(getResources().getString(R.string.theta_ip_address));
            HttpConnector.ShootResult result = camera.takePicture(postviewListener);

            return result;
        }

        @Override
        protected void onPostExecute(HttpConnector.ShootResult result) {
            if (result == HttpConnector.ShootResult.FAIL_CAMERA_DISCONNECTED) {
                Log.i(TAG, "takePicture:FAIL_CAMERA_DISCONNECTED");
            } else if (result == HttpConnector.ShootResult.FAIL_STORE_FULL) {
                Log.i(TAG, "takePicture:FAIL_STORE_FULL");
            } else if (result == HttpConnector.ShootResult.FAIL_DEVICE_BUSY) {
                Log.i(TAG, "takePicture:FAIL_DEVICE_BUSY");
            } else if (result == HttpConnector.ShootResult.SUCCESS) {
                Log.i(TAG, "takePicture:SUCCESS");
            }

            mActivity.hideProgress();
        }

        private class CaptureListener implements HttpEventListener {
            private String latestCapturedFileId;
            private boolean ImageAdd = false;

            @Override
            public void onCheckStatus(boolean newStatus) {
                if(newStatus) {
                    mActivity.showProgress("takePicture:FINISHED");
                    Log.i(TAG, "takePicture:FINISHED");
                } else {
                    mActivity.showProgress("takePicture:IN PROGRESS");
                    Log.i(TAG, "takePicture:IN PROGRESS");
                }
            }

            @Override
            public void onObjectChanged(String latestCapturedFileId) {
                this.ImageAdd = true;
                this.latestCapturedFileId = latestCapturedFileId;
                Log.i(TAG, "ImageAdd:FileId " + this.latestCapturedFileId);
            }

            @Override
            public void onCompleted() {
                Log.i(TAG, "CaptureComplete");
                if (ImageAdd) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mCaptureButton.setEnabled(true);
                            Log.i(TAG, "textCameraStatus text_camera_standby");
                            mActivity.continueAfterCapture(latestCapturedFileId);


//                            new GetThumbnailTask(latestCapturedFileId).execute();
                        }
                    });
                }
            }

            @Override
            public void onError(String errorMessage) {
                Log.i(TAG, "CaptureError " + errorMessage);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mCaptureButton.setEnabled(true);
                        Log.i(TAG, "textCameraStatus text_camera_standby");
                    }
                });
            }
        }

    }


    private class GetThumbnailTask extends AsyncTask<Void, String, Void> {

        private String fileId;

        public GetThumbnailTask(String fileId) {
            this.fileId = fileId;
        }

        @Override
        protected Void doInBackground(Void... params) {
            HttpConnector camera = new HttpConnector(getResources().getString(R.string.theta_ip_address));
            Bitmap thumbnail = camera.getThumb(fileId);
//            camera.getImage()
            if (thumbnail != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] thumbnailImage = baos.toByteArray();
                int x = 0;
//                GLPhotoActivity.startActivityForResult(ImageListActivity.this, cameraIpAddress, fileId, thumbnailImage, true);
            } else {
                publishProgress("failed to get file data.");
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            for (String log : values) {
                Log.i(TAG, log);
            }
        }
    }



}
