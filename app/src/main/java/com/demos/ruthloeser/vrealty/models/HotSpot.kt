package com.demos.ruthloeser.vrealty.models

import java.io.Serializable

/**
 * Created by ruthloeser on 21/07/2017.
 */
data class HotSpot(val x : Float,
                val y: Float,
                val room: Room) : Serializable