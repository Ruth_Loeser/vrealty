package com.demos.ruthloeser.vrealty.models

import java.io.Serializable

/**
 * Created by ruthloeser on 21/07/2017.
 */
data class RoomList(val rooms : List<Room>) : Serializable