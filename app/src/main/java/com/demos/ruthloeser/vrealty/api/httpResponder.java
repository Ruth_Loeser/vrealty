package com.demos.ruthloeser.vrealty.api;

import java.net.HttpURLConnection;

public interface httpResponder {
    void onDataReceived(String data);
    void onHttpError (HttpURLConnection con);
}



