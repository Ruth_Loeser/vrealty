
package com.demos.ruthloeser.vrealty.api;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class httpAsyncGet extends httpAsyncBase {

    private JSONObject _json;
    private httpResponder _responder;

    public httpAsyncGet(httpResponder responder){
        _responder = responder;
    }
    private void httpGet(final String _url, final httpResponder responder){
                try {
                    String result;
                    URL obj = new URL(_url);
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                    if (_cookiesHeader!=null)
                        load_cookies_into_connection(con);

                    con.setRequestMethod("GET");
                    int responseCode = con.getResponseCode();
                    System.out.println("'GET' request to URL : " + _url);
                    System.out.println("Response Code : " + responseCode);
                    if (responseCode >=400) {
                        responder.onHttpError(con);
                        con.disconnect();
                        return;
                    }
                    System.out.println("Response Body : ");
                    BufferedReader in2 = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();
                    while ((inputLine = in2.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in2.close();
                    result = response.toString();

                    if (_cookiesHeader==null)
                        save_cookies_from_connection(con);

                    responder.onDataReceived(result);

                    con.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
    }

    @Override
    protected Integer doInBackground(String... params) {
        httpGet(params[0],_responder);
        return 0;
    }


    @Override
    protected void onPostExecute(Integer result) {
    }
}


